var AWS = require('aws-sdk');
var express = require('express');
var router = express.Router();

AWS.config.region = process.env.REGION;
var ddb = new AWS.DynamoDB();

var ddbTable =  process.env.STARTUP_SIGNUP_TABLE;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/register',function(req,res,next) {
  var item = {
    email: {'S': req.body.email},
    fullname: {'S': req.body.fullname}
  }
  if(ddb){
    ddb.putItem({
      'TableName': ddbTable,
      'Item': item,
      'Expected': { email: { Exists: false } }
    }, function(err, data){
      if(err){
        var returnStatus = 500;
  
          if (err.code === 'ConditionalCheckFailedException') {
              returnStatus = 409;
          }
          console.log('DDB Error: ' + err);
          res.status(returnStatus).end();
      } else {
        res.send('Thanks for registering!')
      }
    })
  } else {
    console.log('no db object');
    res.send('Sorry no db!');
  }
 
})

module.exports = router;
