# Elastic Beanstalk - Node.js deployment sample

## Prerequisites
This tutorial requires the Node.js language, its package manager called npm, and the Express web application framework. For details on installing these components and setting up your local development environment, see [Setting Up your Node.js Development Environment](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/nodejs-devenv.html).

The tutorial also requires the Elastic Beanstalk Command Line Interface (EB CLI). For details on installing and configuring the EB CLI, see [Install the EB CLI Using Setup Scripts](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install.html) and [Configure the EB CLI](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-configuration.html).

## Initialize Git

The prerequisite Node.js and Express setup results in an Express project structure in the node-express folder. If you haven't already generated an Express project, run the following command. For more details, see [Installing Express](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/nodejs-devenv.html#nodejs-devenv-express).

```
$ express && npm install
```

### To set up a Git repository

1. Initialize the Git repository.

```
$ git init
```

2. Create a file named .gitignore and add the following files and directories to it. These files will be excluded from being added to the repository. This step is not required, but it is recommended.

```
node_modules/
.gitignore
.elasticbeanstalk/
```

## Create an Elastic Beanstalk Environment

Configure an EB CLI repository for your application and create an Elastic Beanstalk environment running the Node.js platform.

1. Create a repository with the eb init command.

```
$ eb init
```

This command creates a configuration file in a folder named .elasticbeanstalk that specifies settings for creating environments for your application, and creates an Elastic Beanstalk application named after the current folder.

2. Create an environment running a sample application with the eb create command.

```
$ eb create
```
This command creates a load balanced environment with the default settings for the Node.js platform and the resources

3. When environment creation completes, use the eb open command to open the environment's URL in the default browser.

## Update the Application

After you have created an environment with a sample application, you can update it with your own application. In this step, we update the sample application to use the Express framework.

### To update your application to use Express

1. On your local computer, create an .ebextensions directory in the top-level directory of your source bundle. In this example, we use node-express/.ebextensions.

2. Add a configuration file that sets the Node Command to "npm start":

node-express/.ebextensions/nodecommand.config

```
option_settings:
  aws:elasticbeanstalk:container:nodejs:
    NodeCommand: "npm start"
```

3. Stage the files:

```
$ git add .
$ git commit -m "First express app"
```

4. Deploy the changes:

```
$ eb deploy
```

5. Once the environment is green and ready, refresh the URL to verify it worked. You should see a web page that says Welcome to Express.

### To configure static files and add a new page to your Express application

1. Add a second entry to the configuration file with the following content:

node-express/.ebextensions/nodecommand.config

```
option_settings:
  ...
  aws:elasticbeanstalk:container:nodejs:staticfiles:
    /public: /public
```

2. Comment out the static mapping in node-express/app.js. This step is not required, but it is a good test to confirm that static mappings are configured correctly.

```
//  app.use(express.static(path.join(__dirname, 'public'))); 
```

## Add a simple form

```html
<!DOCTYPE html>
<head></head>
<body>
    <form method="post" action="/register">
        <input type="text" name="fullname">
        <input type="email" name="email">
        <input type="submit">
    </form>
</body>
```

```javascript
var AWS = require('aws-sdk');
```

Add file .ebextensions/create-dynamodb-table.config

```
Resources:
  StartupSignupsTable:
    Type: AWS::DynamoDB::Table
    Properties:
      KeySchema:
        HashKeyElement: {AttributeName: email, AttributeType: S}
      ProvisionedThroughput: {ReadCapacityUnits: 1, WriteCapacityUnits: 1}
```

### Add environment variables

```
aws:elasticbeanstalk:application:environment:
    AWS_REGION: '`{"Ref" : "AWS::Region"}`'
    STARTUP_SIGNUP_TABLE: '`{"Ref" : "StartupSignupsTable"}`'
```

### Connect to the DynamoDB

```javascript
AWS.config.region = process.env.REGION;
var ddb = new AWS.DynamoDB();
var ddbTable =  process.env.STARTUP_SIGNUP_TABLE;
```

### Implement method to register user submition
```javascript
router.post('/register',function(req,res,next) {
  var item = {
    email: {'S': req.body.email},
    fullname: {'S': req.body.fullname}
  }
  if(ddb){
    ddb.putItem({
      'TableName': ddbTable,
      'Item': item,
      'Expected': { email: { Exists: false } }
    }, function(err, data){
      if(err){
        var returnStatus = 500;
  
          if (err.code === 'ConditionalCheckFailedException') {
              returnStatus = 409;
          }
          console.log('DDB Error: ' + err);
          res.status(returnStatus).end();
      } else {
        res.send('Thanks for registering!')
      }
    })
  } else {
    console.log('no db object');
    res.send('Sorry no db!');
  }
 
})
``` 

Add AmazonDynamoDBFullAccess to  aws-elasticbeanstalk-ec2-role role in [roles pages](https://console.aws.amazon.com/iam/home#roles)



